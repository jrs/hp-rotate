CC=gcc
CFLAGS=-Wall -lX11 -lXrandr -I. -O2

all: hp-rotate.o rotate-xrandr.o
	$(CC) -o hp-rotate hp-rotate.o rotate-xrandr.o $(CFLAGS)

clean:
	rm *.o hp-rotate

install:
	cp hp-rotate $(DESTDIR)/usr/bin/hp-rotate
	cp 62-rotation.rules $(DESTDIR)/etc/udev/rules.d/62-rotation.rules
	
	
