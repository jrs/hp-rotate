/*
 * Places a blocking read on the event file, 
 * identifies incoming event types,
 * then calls the relevant function.
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <linux/input.h>
#include <unistd.h>
#include "rotate-xrandr.h"
#define ROTATEFILE "/dev/input/hp-rotate"

int main()
{
    int fd;
    struct input_event rotate;
    
    if((fd = open(ROTATEFILE, O_RDONLY)) == -1)
    {
        printf("Failed to open "ROTATEFILE".\n");
        exit(EXIT_FAILURE);
    }
    else
    {
        printf("Opened device:"ROTATEFILE"\n");
    }

    printf("Waiting for HP WMI to report.\n");
    
    while(1)
    {
		(void)read(fd, &rotate, sizeof(struct input_event));
		if (rotate.type == 5)
		{	
					
			if (rotate.value == 1)
			{
				printf("Tablet mode\n");
				hinge(8);
			}
			
			if (rotate.value == 0)
			{
				printf("Normal mode\n");
				hinge(1);
			}
			
		}
		if (rotate.type == 4)	
		{
			printf("Button pressed");
			
			button();
		}

    }

return 0;
}
