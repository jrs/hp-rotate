/*
 * This is the header for rotate-xrandr.c
 */

#ifndef ROTATEBUTTON_H_INCLUDED
#define ROTATEBUTTON_H_INCLUDED

int button();
int hinge();
void freeresources();
void getinfo();

#endif
