This is a simple program written in C designed to make the screen of the 
HP elitebook 2760p automatically rotate when it is physically moved into
tablet mode. It should also handle the rotation button on the side.

It is similar in function to magick-rotation.
