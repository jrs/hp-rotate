/*
 * Various functions to interface with xrandr
 * Build with -lX11 -lXrandr options
 */

#include "rotate-xrandr.h"
#include <X11/Xlib.h>
#include <X11/extensions/Xrandr.h>

Rotation current_ro;
Display *dpy;
XRRScreenConfiguration *sc;
Time conftime;
Time now;
Window root;

int hinge(int orientation) {
	getinfo();
	
	XRRSetScreenConfig(dpy, sc, root, 0, orientation, now); 
	freeresources();
	
	return 0;
}
	
int button() {
	
	getinfo();

	XRRRotations(dpy, 0, &current_ro);
	if (current_ro == 1)
	{
		XRRSetScreenConfig(dpy, sc, root, 0, 8, now); 
	}
	else
	{
		XRRSetScreenConfig(dpy, sc, root, 0, 1, now); 
	}
	
	freeresources();

	return 0;
}

void freeresources()
{
	XRRFreeScreenConfigInfo (sc);
	XCloseDisplay(dpy);
}

void getinfo()
{
	dpy = XOpenDisplay(NULL);
	root = RootWindow (dpy, 0);
	sc = XRRGetScreenInfo (dpy, root);
	now = XRRTimes(dpy, 0, &conftime);
}
	
